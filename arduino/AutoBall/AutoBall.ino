﻿/*
Name:    AutoBall.ino
Created: 2018-05-21 오후 3:28:22
Author:  kphk4
ETC :  오토볼 펌웨어
*/

#define HORIZONTAL_AXIS_MOTOR_ENABLE 5
#define HORIZONTAL_AXIS_MOTOR_INT1 11
#define HORIZONTAL_AXIS_MOTOR_INT2 12

#define VERTICAL_AXIS_MOTOR_ENABLE 6
#define VERTICAL_AXIS_MOTOR_INT3 10
#define VERTICAL_AXIS_MOTOR_INT4 9

#define BLUETOOTH_RX 2
#define BLUETOOTH_TX 3

#define LED0_PIN A0
#define LED1_PIN A1
#define LED2_PIN A2
#define LED3_PIN A3
#define LED4_PIN A4
#define LED5_PIN A5
#define LED6_PIN A6
#define LED7_PIN A7

#define ANALOG_HIGH 1024
#define ANALOG_LOW 0

#include <SoftwareSerial.h>

SoftwareSerial mySerial(BLUETOOTH_RX, BLUETOOTH_TX);

unsigned char contrlStatsBit = 0x00;

signed char verticalMotorStats = 0; //MoveAdvance = 1, MoveJunior = -1, stop = 0
signed char horizontalMotorStats = 0; //MoveRight = 1, MoveLeft = -1, enable = 0

unsigned char verticalContrl = 0x00;
unsigned char horizontalContrl = 0x00;
unsigned char modeContrl = 0x00;

enum Mode
{
	Mode1,
	Mode2,
	Mode3,
	Mode4
};

void setup()
{
	pinMode(HORIZONTAL_AXIS_MOTOR_ENABLE, OUTPUT);
	pinMode(HORIZONTAL_AXIS_MOTOR_INT1, OUTPUT);
	pinMode(HORIZONTAL_AXIS_MOTOR_INT2, OUTPUT);

	pinMode(VERTICAL_AXIS_MOTOR_ENABLE, OUTPUT);
	pinMode(VERTICAL_AXIS_MOTOR_INT3, OUTPUT);
	pinMode(VERTICAL_AXIS_MOTOR_INT4, OUTPUT);

	digitalWrite(HORIZONTAL_AXIS_MOTOR_ENABLE, HIGH);
	digitalWrite(VERTICAL_AXIS_MOTOR_ENABLE, HIGH);

	StopHorizontal();
	StopVertical();

	LedMode(Mode1);

	mySerial.begin(9600);
	Serial.begin(9600);

}

void loop()
{
	if (mySerial.available())
	{
		unsigned char statsBit = mySerial.read();
		verticalContrl = (statsBit & 0x0C) >> 2;
		horizontalContrl = (statsBit & 0x03);
		modeContrl = (statsBit & 0xE0);
		Serial.print("oign : ");
		Serial.println(statsBit, BIN);
		Serial.print("verticalContrl : ");
		Serial.println(verticalContrl, BIN);
		Serial.print("horizontalContrl : ");
		Serial.println(horizontalContrl, BIN);
		Serial.print("modeContrl : ");
		Serial.println(modeContrl, BIN);
		switch (verticalContrl)
		{
		case 0x01:
			Serial.println("a");
			MoveRight();
			break;
		case 0x02:
			Serial.println("b");
			MoveLeft();
			break;
		default:
			Serial.println("c");
			StopVertical();
			break;
		}

		switch (horizontalContrl)
		{
		case 0x01:
			Serial.println("d");
			MoveAdvance();
			break;
		case 0x02:
			Serial.println("e");
			MoveJunior();
			break;
		default:
			Serial.println("f");
			StopHorizontal();
			break;
		}

		switch (modeContrl)
		{
		case 0x01:
			LedMode(Mode1);
			break;
		case 0x02:
			LedMode(Mode2);
			break;
		case 0x03:
			LedMode(Mode3);
			break;
		case 0x04:
			LedMode(Mode4);
			break;
		}
	}
}

void MoveAdvance()
{
	horizontalMotorStats = 1;
	digitalWrite(HORIZONTAL_AXIS_MOTOR_ENABLE, HIGH);
	digitalWrite(HORIZONTAL_AXIS_MOTOR_INT1, HIGH);
	digitalWrite(HORIZONTAL_AXIS_MOTOR_INT2, LOW);

}

void MoveJunior()
{
	horizontalMotorStats = -1;
	digitalWrite(HORIZONTAL_AXIS_MOTOR_ENABLE, HIGH);
	digitalWrite(HORIZONTAL_AXIS_MOTOR_INT1, LOW);
	digitalWrite(HORIZONTAL_AXIS_MOTOR_INT2, HIGH);

}

void MoveRight()
{
	verticalMotorStats = 1;
	digitalWrite(VERTICAL_AXIS_MOTOR_ENABLE, HIGH);
	digitalWrite(VERTICAL_AXIS_MOTOR_INT3, HIGH);
	digitalWrite(VERTICAL_AXIS_MOTOR_INT4, LOW);
}

void MoveLeft()
{
	verticalMotorStats = -1;
	digitalWrite(VERTICAL_AXIS_MOTOR_ENABLE, HIGH);
	digitalWrite(VERTICAL_AXIS_MOTOR_INT3, LOW);
	digitalWrite(VERTICAL_AXIS_MOTOR_INT4, HIGH);
}

void StopHorizontal()
{
	horizontalMotorStats = 0;
	digitalWrite(HORIZONTAL_AXIS_MOTOR_ENABLE, LOW);
}

void StopVertical()
{
	verticalMotorStats = 0;
	digitalWrite(VERTICAL_AXIS_MOTOR_ENABLE, LOW);
}

void LedMode(signed char i)
{
	switch (i)
	{
	case Mode1:
		analogWrite(LED0_PIN, ANALOG_HIGH);
		analogWrite(LED1_PIN, ANALOG_LOW);
		analogWrite(LED2_PIN, ANALOG_HIGH);
		analogWrite(LED3_PIN, ANALOG_LOW);
		analogWrite(LED4_PIN, ANALOG_HIGH);
		analogWrite(LED5_PIN, ANALOG_LOW);
		analogWrite(LED6_PIN, ANALOG_HIGH);
		analogWrite(LED7_PIN, ANALOG_LOW);
		break;

	case Mode2:
		analogWrite(LED0_PIN, ANALOG_LOW);
		analogWrite(LED1_PIN, ANALOG_HIGH);
		analogWrite(LED2_PIN, ANALOG_LOW);
		analogWrite(LED3_PIN, ANALOG_HIGH);
		analogWrite(LED4_PIN, ANALOG_LOW);
		analogWrite(LED5_PIN, ANALOG_HIGH);
		analogWrite(LED6_PIN, ANALOG_LOW);
		analogWrite(LED7_PIN, ANALOG_HIGH);
		break;

	case Mode3:
		analogWrite(LED0_PIN, ANALOG_HIGH);
		analogWrite(LED1_PIN, ANALOG_LOW);
		analogWrite(LED2_PIN, ANALOG_LOW);
		analogWrite(LED3_PIN, ANALOG_HIGH);
		analogWrite(LED4_PIN, ANALOG_HIGH);
		analogWrite(LED5_PIN, ANALOG_LOW);
		analogWrite(LED6_PIN, ANALOG_LOW);
		analogWrite(LED7_PIN, ANALOG_HIGH);
		break;

	case Mode4:
		analogWrite(LED0_PIN, ANALOG_HIGH);
		analogWrite(LED1_PIN, ANALOG_HIGH);
		analogWrite(LED2_PIN, ANALOG_HIGH);
		analogWrite(LED3_PIN, ANALOG_HIGH);
		analogWrite(LED4_PIN, ANALOG_HIGH);
		analogWrite(LED5_PIN, ANALOG_HIGH);
		analogWrite(LED6_PIN, ANALOG_HIGH);
		analogWrite(LED7_PIN, ANALOG_HIGH);
		break;

	default:
		analogWrite(LED0_PIN, ANALOG_LOW);
		analogWrite(LED1_PIN, ANALOG_LOW);
		analogWrite(LED2_PIN, ANALOG_LOW);
		analogWrite(LED3_PIN, ANALOG_LOW);
		analogWrite(LED4_PIN, ANALOG_LOW);
		analogWrite(LED5_PIN, ANALOG_LOW);
		analogWrite(LED6_PIN, ANALOG_LOW);
		analogWrite(LED7_PIN, ANALOG_LOW);
		break;
	}
}